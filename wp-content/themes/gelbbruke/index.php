<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

get_header();
?>

	<main id="primary" class="site-main">
	
	<?php //Show homepage categories on homepage
		$args = array(
			'post_type' => 'post',
			'order' => 'ASC',
			'orderby' => 'date',
			'posts_per_page' => -1,
			'category_name' => 'home',
			'paged' => get_query_var('paged'),
			'post_parent' => $parent
		); 
		$q = new WP_Query($args);
		if ( $q -> have_posts() ) { 
			if ( is_home() && ! is_front_page() ) {
				echo 'is home not fp';
			}
			while ( $q -> have_posts() ) {
				$q -> the_post();
				//The loop
				get_template_part( 'template-parts/content-homepage', get_post_type() );
			}
		} else {
			get_template_part( 'template-parts/content-homepage', 'none' );
		}
		?>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
