<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

?>

<div class="homepage-block <?php add_tags_as_class(); gb_apply_theme( $post->ID ); ?>">
	<?php if ( has_post_thumbnail() ) {
		echo '<div class="picture-block">';
		echo '<picture>';
		the_post_thumbnail('full');
		echo '</picture>';
		echo '</div>';
	} ?>
	<div class="content">
		<?php the_title('<h2>', '</h2>'); ?>
		<?php the_content(); ?>
	</div>
</div>