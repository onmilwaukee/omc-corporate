<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
					<?php
					include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
					if ( is_plugin_active('gb-theme-enhancer/gb-theme-enhancer.php') ) {
						$gb_archive = $wp_query->get_queried_object();
						gb_display_cat_image( $gb_archive );
						echo '<div class="title-box">';
						gb_display_cat_title( $gb_archive, '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
						echo '<div>';
					} else {
						echo '<div class="title-box">';
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
						echo '<div>';
					}
					?>
				</div><!-- .title-box -->
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-excerpt', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
