<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

get_header();
?>

	<main id="primary" class="site-main">
		<?php
		if ( is_page('our-team') ) { ?>
			<div class="the-content content-team">
			<article class="page gb-team">
				<header class="entry-header">
					<div class="title-box">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</header>
				<div class="entry-content">
				<?php
				$argsTeam = array(
					'post_type' => 'gb-team',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'meta_value_num',
					'meta_key' => 'gb_teamrel_key'
					//'category_name' => 'home',
					//'paged' => get_query_var('paged'),
					//'post_parent' => $parent
				); 
				$q1 = new WP_Query($argsTeam);

				if ( $q1 -> have_posts() ) { 
					$content = apply_filters( 'the_content', get_the_content() );
					echo $content;
					
					echo '<div class="omcorp-team-members">';
					while ( $q1 -> have_posts() ) {

						$q1 -> the_post();
						//The loop
						get_template_part( 'template-parts/content-team', get_post_type() );
					}
				} ?>
					</div><!-- .omcorp-team-members-->
				</div><!-- .entry-content -->
			</article>
			<?php 
		}

		else {

			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content', 'page' );
	
				// If comments are open or we have at least one comment, load up the comment template.
				// if ( comments_open() || get_comments_number() ) :
				// 	comments_template();
				// endif;
	
			endwhile; // End of the loop.
		}
		?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
