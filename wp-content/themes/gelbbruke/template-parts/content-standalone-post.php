<?php
/**
 * Template part for displaying job openings
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

?>
<div class="gb-indicator">standalone-post</div>
<?php gelbbruke_post_thumbnail(); ?>

<div class="the-content content-standalone-post">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<div class="category-header">
				<?php gb_display_category_list(); ?>
			</div>
			<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
			?>
		</header><!-- .entry-header -->

		

		<div class="entry-content">
			<?php
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gelbbruke' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			?>
		</div><!-- .entry-content -->

		<footer class="entry-footer">
			<!-- <a href="jobs" title="Employement Opportunities">See more employment opportunities.</a> -->
			<?php gelbbruke_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-<?php the_ID(); ?> -->
</div><!-- .the-content-->