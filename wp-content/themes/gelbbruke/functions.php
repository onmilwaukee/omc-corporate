<?php
/**
 * gelbbruke functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gelbbruke
 */

if ( ! defined( 'omcorp_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'omcorp_VERSION', '1.23.0322' );
}

$styleVer = '1250';

if ( ! function_exists( 'gelbbruke_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function gelbbruke_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on gelbbruke, use a find and replace
		 * to change 'gelbbruke' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'gelbbruke', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'gelbbruke' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'gelbbruke_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'gelbbruke_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gelbbruke_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gelbbruke_content_width', 640 );
}
add_action( 'after_setup_theme', 'gelbbruke_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gelbbruke_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'gelbbruke' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'gelbbruke' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'gelbbruke_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gelbbruke_scripts() {
	wp_enqueue_style( 'gelbbruke-style', get_stylesheet_uri(), array(), omcorp_VERSION . '-' . $styleVer );
	wp_style_add_data( 'gelbbruke-style', 'rtl', 'replace' );

	wp_enqueue_script( 'gelbbruke-navigation', get_template_directory_uri() . '/js/navigation.js', array(), omcorp_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gelbbruke_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/* ==============================================



			gelbbruke



============================================== */

//Enable a stylesheet for the back end
add_theme_support('editor-styles');
add_editor_style( '/inc/css/editor-styles.css' );

function gelbbruke_setup_theme() {
	//Update thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'gb-square', 800, 800, true);
}
add_action( 'after_setup_theme', 'gelbbruke_setup_theme');

function add_tags_as_class() {
	$posttags = get_the_tags();
	if ($posttags) {
		foreach( $posttags as $tag) {
			echo ' tag-' . $tag->name . ' ';
		}
	}
}

function gb_echo_category($post) {
	$postcats = get_the_category_list('gb-cat-', '', $post);
	if ($postcats) {
		foreach( $postcats as $cat) {
			echo $cat->name;
		}
	}
}

/* -------- Remove comments in its entirety ------------ */
//https://www.powderkegwebdesign.com/quickly-easily-remove-comments-backend-wordpress/

// Removes from admin menu
add_action( 'admin_menu', 'pk_remove_admin_menus' );
function pk_remove_admin_menus() {
	remove_menu_page( 'edit-comments.php' );
}

// Removes from post and pages
add_action('init', 'pk_remove_comment_support', 100);
function pk_remove_comment_support() {
	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );
}

// Removes from admin bar
add_action( 'wp_before_admin_bar_render', 'pk_remove_comments_admin_bar' );
function pk_remove_comments_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}




function the_secondary_logo() {
	if ( get_theme_mod( 'gb_secondary_logo' ) ) : ?>
		<a href="<?php echo get_home_url() ?>" title="Home" class="gb-secondary-logo">
			<img src="<?php echo get_theme_mod( 'gb_secondary_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
		</a>
		<?php else: 
			the_custom_logo();
	endif;
}

function the_background_videos() {
	if ( get_theme_mod( 'gb_background_video' ) ) : ?>
		<video class="vertical" src="<?php echo get_theme_mod( 'gb_background_video' ) ?>" muted loop autoplay playsinline="true" disablePictureInPicture="true"></video>
	<?php endif;
	
	if ( get_theme_mod( 'gb_background_video_desktop') ) : ?>
		<video class="landscape" src="<?php echo get_theme_mod( 'gb_background_video_desktop' ) ?>" muted loop autoplay playsinline="true" disablePictureInPicture="true"></video>
	<?php endif;
}

function display_background_video() {
	if ( get_theme_mod( 'gb_background_video' ) || get_theme_mod( 'gb_background_video_desktop' ) ) : ?>
		<div class="video-bg">
			<?php the_background_videos() ?>
		</div>
	<?php endif;
}

function gb_display_category_list() {
	$categories_list = get_the_category_list( esc_html__( ', ', 'gelbbruke' ) );
	if ( $categories_list ) {
		/* translators: 1: list of categories. */
		printf( '<span class="cat-links">' . esc_html__( '%1$s', 'gelbbruke' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}