<?php
/**
 * Template part for displaying archive excerpts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

?>

<div class="the-content content-excerpt">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php echo the_permalink() ?>">
			<header class="entry-header">
				<?php
					if ( is_singular() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title">', '</h2>' );
					endif;
					the_excerpt(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'gelbbruke' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							wp_kses_post( get_the_title() )
						)
					);
				?>
			</header><!-- .entry-header -->

			

			<div class="entry-content">
				<picture><?php gelbbruke_post_thumbnail(); ?></picture>
				
				
			</div><!-- .entry-content -->

			<footer class="entry-footer">
				<!-- <a href="jobs" title="Employement Opportunities">See more employment opportunities.</a> -->
				<?php gelbbruke_entry_footer(); ?>
			</footer><!-- .entry-footer -->
		</a>
		</article><!-- #post-<?php the_ID(); ?> -->
	</div><!-- .the-content -->

<div class="gb-indicator">excerpt</div>