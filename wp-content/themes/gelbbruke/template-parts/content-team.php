<?php
/**
 * Template part for displaying team members
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gelbbruke
 */

?>

<?php 
    // Get 'employee' posts
    // $team_employees = get_posts( array(
    //     'post_type' => 'gb-team',
    //     'posts_per_page' => -1,
    //         'meta_query' => array (
    //                 array (
    //                         'key' => 'gb_teamlastname',
    //         ),
    //         array (
    //             'key' => 'gb_teamrel',
    //             'value' => '2',
    //             'compare' => 'LIKE'
    //         )
    //     ),
    //     'meta_key' => 'gb_teamlastname',
    //     'orderby' => 'meta_value',
    //     'order' => 'ASC'
    // ) );
?>
<div class="gb-indicator">team</div>
<div class="team-block">
    <?php
        $gb_post_id = get_the_ID();
        $gb_name = the_title('', '', false);
        //$gb_headshot_id = get_post_meta( $gb_post_id, 'teamheadshot', true );
        //$gb_headshot_src = wp_get_attachment_image_src($gb_headshot_id, 'gb-square');
        $gb_headshot_src = get_the_post_thumbnail_url( $gb_post_id, 'gb-square');
        $gb_title = get_post_meta( $gb_post_id, 'gb_teamtitle_key', true );
        $gb_email = get_post_meta( $gb_post_id, 'gb_teamemail_key', true );
        $gb_bio = get_post_meta( $gb_post_id, 'gb_teambio_key', true );
        $gb_twitter = get_post_meta( $gb_post_id, 'gb_teamtwitter_key', true );
        $gb_facebook = get_post_meta( $gb_post_id, 'gb_teamfacebook_key', true );
        $gb_instagram = get_post_meta( $gb_post_id, 'gb_teaminstagram_key', true );
        $gb_linkedin = get_post_meta( $gb_post_id, 'gb_teamlinkedin_key', true );
    
        the_title('<h2>', '</h2>');

        echo '<picture class="team-headshot">';
        echo '<img src="' . $gb_headshot_src . '" />';
        echo '</picture>';

    ?>
    <div class="team-details">
        <span class="employee-title"><?php echo $gb_title ?></span>
        <span><a href="mailto:<?php echo $gb_email ?>"><?php echo $gb_email ?></a></span>
        <ul class="employee-social">
            <?php if ( !empty( $gb_twitter ) ) {
                echo '<li class="twitter"><a href="https://twitter.com/' . $gb_twitter . '" alt="Follow ' . $gb_name . ' on Twitter." >
                    <picture am_icon="white">
                        <svg version="1.1" id="twitter-icon-white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 179 145" style="enable-background:new 0 0 179 145;" xml:space="preserve"><style type="text/css">#twitter-icon-white .f{fill:#FFFFFF;}</style><path class="f" d="M4,127c0,0,27,5,52-15c0,0-24,0-33-24c0,0,6,1,15-1c0,0-26-5-28-35 c0,0,7,4,16,5c0,0-26-16-11-47c0,0,26,35,73,37c0,0-6-18,9-33c15-15,38-12,50,1 c0,0,13-2,22-8c0,0-3,13-15,19c0,0,9-0,20-5c0,0-9,13-18,18c0,0,5,45-35,81 C83,155,27,144,4,127z"></path></svg>
                    </picture>
                </a></li>';
            } ?>
            <?php if ( !empty( $gb_facebook ) ) {
                echo '<li class="facebook"><a href="https://facebook.com/' . $gb_facebook . '" alt="Follow ' . $gb_name . ' on Facebook." >
                    <picture am_icon="white">
                        <svg version="1.1" id="facebook-icon-white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1024 1024"><style type="text/css">#facebook-icon-white .f{fill:#FFFFFF;}</style><g><path class="f" d="M1024,512C1024,229,795,0,512,0S0,229,0,512c0,256,187,467,432,506V660H302V512h130V399 C432,271,508,200,625,200c56,0,115,10,115,10v126h-65c-64,0-83,40-83,80v96h142l-23,148H592v358 C837,979,1024,768,1024,512z"></path></g></svg>
                    </picture>
                </a></li>';
            } ?>
            <?php if ( !empty( $gb_instagram ) ) {
                echo '<li class="instagram"><a href="https://instagram.com/' . $gb_instagram . '" alt="Follow ' . $gb_name . ' on Instagram." >
                    <picture am_icon="white">
                        <svg version="1.1" id="instagram-icon-white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 600 600"><style type="text/css">#instagram-icon-white .f{fill:#FFFFFF;}</style><path class="f" d="M299.5,452.9c-84.4,0-153.1-68.7-153.1-153.1s68.7-153.1,153.1-153.1s153.1,68.7,153.1,153.1  S384,452.9,299.5,452.9z M299.5,200.7c-54.6,0-99.1,44.4-99.1,99.1s44.4,99.1,99.1,99.1s99.1-44.4,99.1-99.1  S354.2,200.7,299.5,200.7z"></path><path class="f" d="M403.3,595.2H197c-66,0-113.8-16.4-146.1-50c-32.6-34-48.5-84.8-48.5-155.4V209.2c0-70.6,15.9-121.4,48.5-155.4  C83.2,20.1,131,3.7,197,3.7h206.3c66,0,113.8,16.4,146.1,50c32.6,34,48.5,84.8,48.5,155.4v180.5c0,70.6-15.9,121.4-48.5,155.4  C517.1,578.8,469.3,595.2,403.3,595.2z M197,58.6c-100.6,0-139.7,42.2-139.7,150.6v180.5C57.3,498,96.5,540.3,197,540.3h206.3  C503.9,540.3,543,498,543,389.7V209.2c0-108.3-39.2-150.6-139.7-150.6H197z"></path><circle class="f" cx="457.4" cy="141.8" r="35.8"></circle></svg>
                    </picture>
                </a></li>';
            } ?>
            <?php if ( !empty( $gb_linkedin ) ) {
                echo '<li class="linkedin"><a href="https://linkedin.com/in/' . $gb_linkedin . '" alt="Follow ' . $gb_name . ' on LinkedIn." >
                    <picture am_icon="white">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 163.1 162.2"><style type="text/css">.st0{fill:#FFFFFF;}</style><g><ellipse class="st0" cx="19.8" cy="19.5" rx="19.8" ry="19.5"/><rect x="2.7" y="53.8" class="st0" width="33.9" height="108.4"/></g><path class="st0" d="M129,162.2h34.1v-59.7c0-41.5-15.9-51.1-40.9-51.1c-23.5,0-31.3,17.5-31.3,17.5h-0.6V53.8H58v108.4h33.8v-55.5 c0-18,7.6-25.8,19-25.8c11.4,0,18.3,6.5,18.3,24.8V162.2z"/></svg>
                    </picture>
                </a></li>';
            } ?>
        </ul>
    </div>

    <p class="employee-bio"><?php echo $gb_bio ?></p>
    
</div>