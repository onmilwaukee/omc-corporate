<?php
/**
 * gelbbruke Theme Customizer
 *
 * @package gelbbruke
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function gelbbruke_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'gelbbruke_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'gelbbruke_customize_partial_blogdescription',
			)
		);
	}
}
add_action( 'customize_register', 'gelbbruke_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function gelbbruke_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function gelbbruke_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function gelbbruke_customize_preview_js() {
	wp_enqueue_script( 'gelbbruke-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), gelbbruke_VERSION, true );
}
add_action( 'customize_preview_init', 'gelbbruke_customize_preview_js' );




//https://getflywheel.com/layout/how-to-add-options-to-the-wordpress-customizer/
function gb_add_secondary_logo($wp_customize) {
	// add a setting for the site logo
	$wp_customize -> add_setting('gb_secondary_logo');
	// Add a control to upload the logo
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'gb_secondary_logo',
		array(
			'label' => 'Upload secondary logo',
			'section' => 'title_tagline',
			'settings' => 'gb_secondary_logo'
	) ) );
}
add_action('customize_register', 'gb_add_secondary_logo');

function gb_add_background_video($wp_customize) {
	$wp_customize -> add_setting('gb_background_video');
	$wp_customize -> add_control( 'gb_background_video',
		array(
			'type' => 'text',
			'label' => 'Background video URL (mobile)',
			'section' => 'background_image',
			'settings' => 'gb_background_video'
	) );
}
add_action('customize_register', 'gb_add_background_video');

function gb_add_background_video_desktop($wp_customize) {
	$wp_customize -> add_setting('gb_background_video_desktop');
	$wp_customize -> add_control( 'gb_background_video_desktop',
		array(
			'type' => 'text',
			'label' => 'Background video URL (desktop)',
			'section' => 'background_image',
			'settings' => 'gb_background_video_desktop'
	) );
}

add_action('customize_register', 'gb_add_background_video_desktop');