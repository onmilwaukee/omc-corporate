<?php
   /*
   Plugin Name: Gelbruke Theme Enhancer
   Plugin URI: https://corporate.onmilwaukee.com/gelbruke-theme
   description: Adds Gelbruke Team Profiles and other useful fields for the Gelbruke theme.
   Version: 0.1
   Author: Jason McDowell
   Author URI: https://littletinyfish.com
   License: GPL2
   */

//Enable a shittier console.log
function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script type="text/javascript">' . $js_code . '</script>';
    }
	echo $js_code;
}

$post_id = $_GET['post'];
//console_log("gb_id24 = " . $post_id);

// =================== CREATES GB-TEAM POST TYPE ====================

function gb_team_post_type() {
   
	// Labels
	$labels = array(
		'name' => _x("Gelbruke Team Members", "post type general name"),
		'singular_name' => _x("Gelbruke Team Member", "post type singular name"),
		'menu_name' => 'Gelbruke Team Members',
		'add_new' => _x("Add New Member", "team item"),
		'add_new_item' => __("Add new member profile"),
		'edit_item' => __("Edit Profile"),
		'new_item' => __("New Profile"),
		'view_item' => __("View Profile"),
		'search_items' => __("Search Profiles"),
		'not_found' =>  __("No Profiles Found"),
		'not_found_in_trash' => __("No Profiles Found in Trash"),
		'parent_item_colon' => ''
	);
	
	// Register post type
	register_post_type('gb-team' , array(
		'labels' => $labels,
		'public' => true,
		'has_archive' => false,
		// 'menu_icon' => get_stylesheet_directory_uri() . '/lib/TeamProfiles/team-icon.png',
		'rewrite' => false,
		'supports' => array('title', 'thumbnail')
	) );
}

add_action( 'init', 'gb_team_post_type', 0 );

// =================== ADD CUSTOM FIELD ====================
if ( is_admin() ) {
	add_action( 'load-post.php', 'init_metabox');
	add_action( 'load-post-new.php', 'init_metabox');
	$styleVer = '010422-0210';
	wp_register_style('gb_team_style', plugins_url('styles.css', __FILE__), array(), $styleVer);
	wp_enqueue_style('gb_team_style');
}

function init_metabox() {
	add_action( 'add_meta_boxes', 'register_custom_fields' );
	add_action( 'save_post', 'gb_save');
}

//Adds meta box
function register_custom_fields() {
	add_meta_box( 
		'gb_teamprofile',       // ID
		__('Team Profile', 'gb_teamprofile_domain' ),  // Title 
		'render_team_fields',		  // Callback
		'gb-team',                    // Screen
		'normal'                      // Context
	);
}

//Compares old and new data
function gb_update_post_meta($post_id, $fieldname, $key) {
	$theEdit = sanitize_text_field( $_POST[$fieldname] );
	$theArchive = get_post_meta( $post_id, $key, true );
	if( $theEdit !== $theArchive ) {
		update_post_meta( $post_id, $key, $theEdit);
	}
}

function gb_save( $post_id ) {
	//https://developer.wordpress.org/reference/functions/add_meta_box/
	$nonce = $_POST['gb_render_team_fields_nonce'];

	if ( ! isset( $nonce ) ) {
		return $post_id;
	}
	if ( ! wp_verify_nonce( $nonce, 'gb_render_team_fields' ) ) {
		return $post_id;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}
	if ( 'gb-team' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
	}
	gb_update_post_meta($post_id, 'gb_teamlastname', 'gb_teamlastname_key');
	gb_update_post_meta($post_id, 'gb_teamtitle', 'gb_teamtitle_key');
	gb_update_post_meta($post_id, 'gb_teamrel', 'gb_teamrel_key');
	gb_update_post_meta($post_id, 'gb_teambio', 'gb_teambio_key');
	gb_update_post_meta($post_id, 'gb_teamemail', 'gb_teamemail_key');
	gb_update_post_meta($post_id, 'gb_teamtwitter', 'gb_teamtwitter_key');
	gb_update_post_meta($post_id, 'gb_teamfacebook', 'gb_teamfacebook_key');
	gb_update_post_meta($post_id, 'gb_teaminstagram', 'gb_teaminstagram_key');
	gb_update_post_meta($post_id, 'gb_teamlinkedin', 'gb_teamlinkedin_key');
}

/* ===========================================

CREATE ALL TEAM FORM FIELDS

============================================*/

// CREATE TEXT and EMAIL FIELDS
function gb_echo_team_textfield( $post, $input_type, $input_name, $label_name, $desc ) {
	$input_value = get_post_meta( $post->ID, $input_name . "_key", true);
	$label_txtdom = get_post_meta( $post->ID, $input_name . "_txtdom", true);
	?>
		<div class="gb_field">
			<label for="<?php echo $input_name ?>"><?php echo __($label_name, $label_txtdom) ?></label>
			<input type="<?php echo $input_type ?>" name="<?php echo $input_name ?>" value="<?php echo $input_value ?>" />
			<?php if ($desc !== '') {
				echo '<div class="gb_desc"><span>' . $desc . '</span></div>';
			} ?>
		</div>
	<?php 
}

// CREATE RADIO BUTTONS
function gb_echo_team_radio( $post, $input_group, $radio_value, $label_name) {
	if( get_post_meta( $post->ID, $input_group . "_key", true) == $radio_value ) {
		$checked = 'checked';
	} ?>
		<div>
			<input type="radio" id="<?php echo $label_name ?>" name="<?php echo $input_group ?>" value="<?php echo $radio_value ?>" <?php echo $checked ?>>
			<label for="<?php echo $label_name ?>"><?php echo $label_name ?></label>
		</div>
	<?php 
}

// CREATE TEXTAREA
function gb_echo_textarea( $post, $label_name, $textarea_name, $rows, $cols, $maxlength, $desc ) {
	$label_txtdom = get_post_meta( $post->ID, $textarea_name . "_txtdom", true);
	$bio = get_post_meta( $post->ID, $textarea_name . '_key', true);
	?>
	<div class="gb_textarea">
		<label for="<?php echo $textarea_name ?>"><?php echo __($label_name, $label_txtdom) ?></label>
		<textarea name="<?php echo $textarea_name ?>" rows="<?php echo $rows ?>" cols="<?php echo $cols ?>" maxlength="<?php echo $maxlength ?>" ><?php echo $bio ?></textarea>
		<?php if (($desc !== '') || ($maxlength !== '')) { ?>
				<div class="gb_desc"><span><?php echo $desc ?> Max length: <?php echo $maxlength ?> characters.</span></div>
			<?php } ?>
	</div>
	<?php 
}

//================ Renders fields in meta box =================
function render_team_fields( $post ) {
	console_log(get_post_meta($post->ID));

	wp_nonce_field( 'gb_render_team_fields', 'gb_render_team_fields_nonce' );
	
	//Render text fields
	gb_echo_team_textfield( $post, 'text', 'gb_teamlastname', 'Last name', '' );
	gb_echo_team_textfield( $post, 'text', 'gb_teamtitle', 'Title', '');

	//Render radio box
	echo '<div class="gb_radio"><label>Relationship</label><div class="radio-group">';
	echo  gb_echo_team_radio( $post, 'gb_teamrel', 1, 'Investor/Owner')
		. gb_echo_team_radio( $post, 'gb_teamrel', 2, 'Employee')
		. gb_echo_team_radio( $post, 'gb_teamrel', 3, 'Partner/Freelancer/Guest')
		. gb_echo_team_radio( $post, 'gb_teamrel', 4, 'Intern')
		. gb_echo_team_radio( $post, 'gb_teamrel', 5, 'Sponsored/Paid/Promoted')
		. gb_echo_team_radio( $post, 'gb_teamrel', 6, 'Released');
	echo '</div>';
	//echo '<div class="gb_desc"><span>Desc</span></div>';
	echo '</div>';
	
	//Render more text fields
	gb_echo_team_textfield( $post, 'email', 'gb_teamemail', 'Email address', 'Don&rsquo;t include the staff part.');
	gb_echo_textarea( $post, 'Short bio', 'gb_teambio', 5, 80, 1000, '');
	gb_echo_team_textfield( $post, 'text', 'gb_teamtwitter', 'Twitter handle', 'Don&rsquo;t include the @.');
	gb_echo_team_textfield( $post, 'text', 'gb_teamfacebook', 'Facebook handle', 'Don&rsquo;t include the /.');
	gb_echo_team_textfield( $post, 'text', 'gb_teaminstagram', 'Instagram handle', 'Don&rsquo;t include the @.');
	gb_echo_team_textfield( $post, 'text', 'gb_teamlinkedin', 'LinkedIn handle', 'Don&rsquo;t include /in.');
}

/* ===========================================

ADD CATEGORY FIELDS

============================================*/
//https://wordpress.stackexchange.com/a/291052

//------ ADD LEAD IMAGE ------
function gb_cat_image_url( $tag ) {
	$gb_cat_image_url = get_term_meta( $tag->term_id, 'gb_cat_image_url', true );
	?>
	<tr class='form-field'>
        <th scope='row'><label for='gb_cat_image_url'><?php _e('Category Image URL'); ?></label></th>
        <td>
            <input type='text' name='gb_cat_image_url' id='gb_cat_image_url' value='<?php echo $gb_cat_image_url ?>'>
            <p class='description'><?php _e('Lead image URL'); ?></p>
			<?php if ($gb_cat_image_url) {
				echo '<figure class="cat-lead-image"><picture><img src="' . $gb_cat_image_url . '" /></picture></figure>';
			} ?>
        </td>
    </tr>
	<?php
}
add_action ( 'category_edit_form_fields', 'gb_cat_image_url');

function gb_save_cat_image( $term_id ) {
	if ( isset( $_POST['gb_cat_image_url'] ) ) {
		update_term_meta( $term_id, 'gb_cat_image_url', $_POST['gb_cat_image_url']);
	}
}
add_action( 'edited_category', 'gb_save_cat_image');

function gb_display_cat_image( $term ) {
	$the_image_url = get_term_meta( $term->term_id, 'gb_cat_image_url', true);
	if ( $the_image_url ) {
		echo '<div class="cat-lead-image">';
			echo '<style type="text/css">.cat-lead-image {background-image: url(' . $the_image_url .');}</style>';
		echo '</div>';
	}
}

//------ ADD CATEGORY TITLE ------
add_action ( 'category_edit_form_fields', function( $tag ){
    $cat_title = get_term_meta( $tag->term_id, '_pagetitle', true ); ?>
    <tr class='form-field'>
        <th scope='row'><label for='cat_page_title'><?php _e('Category Page Title'); ?></label></th>
        <td>
            <input type='text' name='cat_title' id='cat_title' value='<?php echo $cat_title ?>'>
            <p class='description'><?php _e('Title for the Category '); ?></p>
        </td>
    </tr> <?php
});
add_action ( 'edited_category', function( $term_id ) {
    if ( isset( $_POST['cat_title'] ) )
        update_term_meta( $term_id , '_pagetitle', $_POST['cat_title'] );
});

function gb_display_cat_title( $term, $before, $after ) {
	$the_title = get_term_meta( $term->term_id, '_pagetitle', true);
	if ( $the_title ) {
		echo $before . $the_title . $after;
	}
}

//------ ADD HOMEPAGE THEME OPTIONS ------
//https://wptheming.com/2010/08/custom-metabox-for-post-type/
add_action( 'add_meta_boxes', 'gb_add_home_theme_panel' );

function gb_add_home_theme_panel() {
	add_meta_box(
		'gb-home-theme-panel',	//id
		'Home Theme',		//title
		'gb_load_home_theme_panel',	//callback
		'post',			//page
		'side',			//context
		'low'			//priority
	);
}

function isSelected($option, $select) {
	if ( $option === $select ) {
		echo ' selected="selected" ';
	}
}

function gb_load_home_theme_panel( $post ) {
	//global $post;
	wp_nonce_field( basename( __FILE__ ), 'themebox_fields_nonce' );
	$homeTheme = get_post_meta( $post->ID, 'gb-home-themecolor', true);
	$homeLayout = get_post_meta( $post->ID, 'gb-home-layout', true);
	?>
	<select name="gb-home-themecolor">
		<option value="gb-theme-unset" <?php isSelected('gb-theme-unset', $homeTheme) ?> >Unset</option>
		<option value="gb-theme-red" <?php isSelected('gb-theme-red', $homeTheme) ?> >Red</option>
		<option value="gb-theme-blue" <?php isSelected('gb-theme-blue', $homeTheme) ?> >Blue</option>
		<option value="gb-theme-gray" <?php isSelected('gb-theme-gray', $homeTheme) ?> >Gray</option>
		<option value="gb-theme-yellow" <?php isSelected('gb-theme-yellow', $homeTheme) ?> >Yellow</option>
		<option value="gb-theme-white" <?php isSelected('gb-theme-white', $homeTheme) ?> >White</option>
	</select>
	<p>Current theme: <?php echo $homeTheme ?></p>

	<select name="gb-home-layout">
		<option value="gb-layout-unset" <?php isSelected('gb-layout-unset', $homeLayout) ?> >Unset</option>
		<option value="gb-layout-split" <?php isSelected('gb-layout-split', $homeLayout) ?> >Split</option>
		<option value="gb-layout-bigimage" <?php isSelected('gb-layout-bigimage', $homeLayout) ?> >Big Image</option>
		<option value="gb-layout-2col" <?php isSelected('gb-layout-2col', $homeLayout) ?> >Two Columns</option>
	</select>
	<p>Current layout: <?php echo $homeLayout ?></p>
	<?php
}

function gb_save_home_theme_panel($post_id, $post) {
	if ( !current_user_can('edit_post', $post_id ) ) {
		return $post_id;
	}
	if ( !isset( $_POST['gb-home-themecolor'] ) || 
	     !isset( $_POST['gb-home-layout'] ) || 
	     !wp_verify_nonce( $_POST['themebox_fields_nonce'], basename(__FILE__) ) ) {
		return $post_id;
	}
	$gb_hometheme_meta['gb-home-themecolor'] = $_POST['gb-home-themecolor'];
	$gb_hometheme_meta['gb-home-layout'] = $_POST['gb-home-layout'];
	foreach ( $gb_hometheme_meta as $key => $value ) :
		//Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
			return;
		}
		if ( get_post_meta( $post_id, $key, false ) ) {
			//If exists, update value
			update_post_meta( $post_id, $key, $value );
		} else {
			//Otherwise add value
			add_post_meta( $post_id, $key, $value );
		}
		if ( ! $value ) {
			//Delete value if empty
			delete_post_meta( $post_id, $key );
		}
	endforeach;
}

add_action( 'save_post', 'gb_save_home_theme_panel', 1, 2);

function gb_apply_theme( $post_id ) {
	$themeColor = get_post_meta($post_id, 'gb-home-themecolor', true);
	$themeLayout = get_post_meta($post_id, 'gb-home-layout', true);
	echo ' ' . $themeColor . ' ' . $themeLayout . ' ';
}

//To do:
//Add default images
//Make home content type and only apply theme panel to that page
//Add "if plugin exists" to gb_apply_theme on content-homepage
//
?>