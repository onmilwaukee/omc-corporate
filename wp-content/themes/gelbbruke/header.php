<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gelbbruke
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body id="<?php echo 'content-' . $post->post_name; ?>" <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'gelbbruke' ); ?></a>
	<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( '', 'gelbbruke' ); ?>
				<svg version="1.1" id="menu" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 144 144"><style type="text/css">.st0{fill:#FFFFFF;}</style><rect x="0" y="0" class="st0" width="144" height="29"/><rect x="0" y="58" class="st0" width="144" height="29"/><rect x="0" y="115" class="st0" width="144" height="29"/></svg>
			</button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav><!-- #site-navigation -->
	<header id="masthead" class="site-header">
		<?php if ( is_front_page() && is_home() ) :
			display_background_video();
		endif; ?>
		<div class="site-branding">
			<?php if ( is_front_page() && is_home() ) : ?>
				<?php the_custom_logo(); ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				//Secondary logo
				the_secondary_logo(); ?>

				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;

			$gelbbruke_description = get_bloginfo( 'description', 'display' );
			if ( $gelbbruke_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $gelbbruke_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		
	</header><!-- #masthead -->
