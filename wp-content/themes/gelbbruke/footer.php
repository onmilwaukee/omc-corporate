<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gelbbruke
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<!-- <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'gelbbruke' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'gelbbruke' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s via %2$s.', 'gelbbruke' ), 'gelbbruke', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>-->
				<span>&copy; 2023 <a href="https://onmilwaukee.com" title="OnMilwaukee">OnMilwaukee</a>.
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<?php if ( ($post->post_name !== "contact") || ($post->post_name !== "contact-us") ) : ?>
	<div class="contact-bubble"><a title="Contact us" href="contact">
		<svg viewBox="0 0 288 288"><style>.st0{fill:#fff;}.st1{fill:none;stroke:#fff;stroke-width:20;}</style>
			<path class="st0" d="M47.1,48.1v20l194.5,0c1.4,0,2.5,1.1,2.5,2.5v146.3c0,1.4-1.1,2.5-2.5,2.5l-194.5,0c-1.4,0-2.5-1.1-2.5-2.5
				V70.5c0-1.4,1.1-2.5,2.5-2.5V48.1 M47.1,48.1c-12.4,0-22.5,10.1-22.5,22.5v146.3c0,12.4,10.1,22.5,22.5,22.5l194.5,0
				c12.4,0,22.5-10.1,22.5-22.5V70.5c0-12.4-10.1-22.5-22.5-22.5L47.1,48.1L47.1,48.1z"/>
			<polyline class="st1" points="36.2,57.8 144,186.9 251.4,59.6 "/>
		</svg>
	</a></div>
<?php endif ?>

<script src="<?php echo home_url(); ?>/wp-content/themes/gelbbruke/js/countUp.js" type="module"></script>
<!-- <script src="wp-content/themes/gelbbruke/js/gelbbruke.js" type="text/javascript"></script> -->
</body>
</html>
